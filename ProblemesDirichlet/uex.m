function [uanl] = uex(x)

  % The exact solution u 
  % u = x(1-x) or sin(pi*x) 

%  uanl = x.*(1-x) ; 

  uanl = sin(pi*x) ;

end

