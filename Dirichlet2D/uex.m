function [uan] = uex(x,y)
    %uan = 0.5*x.*(x-1).*y.*(y-1);
    uan = sin(pi*x).*sin(pi*y);
end