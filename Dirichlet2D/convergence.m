% Description du comportement de l'erreur
h = [0.25, 0.1, 0.05, 0.02];

% uan = 0.5*x.*(x-1).*y.*(y-1)
erreur = [2.220446049250313e-14, 8.881784197001252e-14, 1.998401444325282e-13, 3.552713678800501e-13];

% erreur pour uan = sin(pi*x).*sin(pi*y)
erreur2 = [5.302928754551495e+00, 8.265416966228623e-01, 2.058706764534124e-01, 3.290517629368495e-02];

% erreur obtenue grace à la consideration du pas h determine a partir de la
% distance euclidienne dans le cas  uan = 0.5*x.*(x-1).*y.*(y-1)
erreur_1 = [6.661338147750939e-14, 5.551115123125783e-14, 1.776356839400250e-13, 6.108649981633556e-13]; 

% erreur obtenue grace à la consideration du pas h determine a partir de la
% distance euclidienne dans le cas  uan = sin(pi*x).*sin(pi*y).
erreur_2 = [2.316291876307974e+00, 4.206846684067234e-01, 1.049725746207431e-01, 1.631715044019819e-02];

% Determination du taux de convergence.
alpha = [];
alpha2 = [];
alpha_1 = [];
alpha_2 = []
for i = 1:3
    alpha = [alpha, log(erreur(i+1)/erreur(i))/log(h(i+1)/h(i))];
    alpha2 = [alpha2, log(erreur2(i+1)/erreur2(i))/log(h(i+1)/h(i))];
    alpha_1 = [alpha_1, log(erreur_1(i+1)/erreur_1(i))/log(h(i+1)/h(i))];
    alpha_2 = [alpha_2, log(erreur_2(i+1)/erreur_2(i))/log(h(i+1)/h(i))];
    %disp(alpha)
end
%alpha
%alpha2
alpha_1
alpha_2