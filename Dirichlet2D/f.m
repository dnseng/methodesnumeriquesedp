function [fd] = f(x,y)
    % la fonction f 
    %fd = -x.*(x - 1) - y.*(y - 1);
    fd = 2*pi^2*sin(pi*x).*sin(pi*y);
end