%
% Resolution numerique d'un probleme aux limites avec un 
% schema de Differences Finies:
% ------------------------------------------------------
% - Laplacien (u) = f in \Omega = ]a, b[x]a, c[
%             	u = 0    sur le bord             
% ------------------------------------------------------
%
% by Dimitri NSENG M2 IMSD FST Nancy.
% ------------------------------------------------------


clear all
format long e

disp('Debut du programme')

a = 0 ;
b = 1 ;
c = 1 ;

% Donnees au bord du domaine 

u = 0 ;

% Appel de la fonction traitant le probleme

dirichlet2d(a, b, c) ;

disp('Fin du programme') 